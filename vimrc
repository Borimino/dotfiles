" Pathogen {{{
filetype off
execute pathogen#infect()
Helptags
filetype plugin indent on
" }}}

" Automatically source .vimrc on saving {{{
autocmd! bufwritepost vimrc :source %
" }}}

" Basic options {{{
set nocompatible
set modelines=0
let mapleader = ","

set tabstop=4
set shiftwidth=4
set softtabstop=4
set textwidth=0

set encoding=utf-8
set scrolloff=3
set autoindent
set showmode
set showcmd
set wildmode=list:longest
set visualbell
set cursorline
set ruler
set backspace=indent,eol,start
set laststatus=2
set number
set relativenumber
set nobackup
set nowritebackup
set noswapfile

set ignorecase
set smartcase
set incsearch
set hlsearch
nnoremap <leader><space> :noh<cr>

set clipboard^=unnamed
set clipboard^=unnamedplus

colorscheme ron

" }}}

" Mappings {{{
"Open the .vimrc in vertical
nnoremap <leader>vi :50vs<cr>:e $MYVIMRC<cr>

"Open new vertical
nnoremap <leader>w <C-w>v<C-w>l
"Move around in vertical
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

"Better indentation
vnoremap < <gv
vnoremap > >gv

"Easy paste/nopaste
nnoremap <leader>p :set paste!<cr>:set relativenumber!<cr>:set number!<cr>

" }}}

" Folding {{{
nnoremap <silent> <Space>  @=(foldlevel('.')?'za':"\<Space>")<cr>

set foldmethod=marker
" }}}

" CtrlP {{{
let g:ctrlp_match_window = 'top,order:ttb'

let g:ctrlp_use_caching = 0
if executable('ag')
	set grepprg=ag\ --nogroup\ --nocolor

	let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
else
	let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files . -co --exclude-standard', 'find %s -type f']
	let g:ctrlp_prompt_mappings = {
		\ 'AcceptSelection("e")': ['<space>', '<cr>', '<2-LeftMouse>'],
		\ }
endif

let g:ctrlp_custom_ignore = { 'dir': '\v[\/](build)$', 'file': '\v\.(class)$'}
" }}}

" Supertab {{{
let g:SuperTabNoCompleteAfter = ['^', '\s']
let g:SuperTabCrMapping = 0 " prevent remap from breaking supertab
let g:SuperTabDefaultCompletionType = 'context'
let g:SuperTabContectDefaulCompletionType = '<c-n>'
let g:SuperTabClosePreviewOnPopupClose = 1 " close scratch window on autocompletion
" }}}

" Java {{{
autocmd FileType java setlocal omnifunc=javacomplete#Complete
let g:syntastic_java_checkers=['javac']
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*

nnoremap <leader>jc :SyntasticJavacEditClasspath<cr>

let g:syntastic_java_javac_config_file_enabled = 1

if filereadable(".project")
	silent !/home/markus/eclipse/java-oxygen/eclipse/eclimd >/dev/null 2>/dev/null &
endif

autocmd VimLeave * :ShutdownEclim
" }}}

" Markdown {{{
augroup ft_markdown
	au!

	au BufNewFile,BufRead *.md setlocal filetype=markdown foldlevel=1

	au Filetype markdown nmap <leader>o :w<cr>:exe "silent !pandoc " . fnameescape(expand('%')) . " -s -o " . fnameescape(expand('%:r')) . ".pdf > /dev/null 2>/dev/null &"<cr>:exe "silent !zathura " . fnameescape(expand('%:r')) . ".pdf > /dev/null 2>/dev/null &"<cr>:redraw!<cr>
" }}}
